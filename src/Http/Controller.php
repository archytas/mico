<?php


namespace Mico\Http;

use PDO;

class Controller
{

    private $pdo;

    /**
     * alias for renderView (more convenient)
     *
     * @param $viewPath
     * @param $params
     */
    public function view($viewPath, $params = []): string
    {
        return $this->renderView($viewPath, $params);
    }

    /**
     * Render the view
     *
     * @param $viewPath
     * @param $params
     * @return string $content
     */
    public function renderView($viewPath, $params = []): string
    {
        extract($params);
        if (substr($viewPath, -4) !== '.php') {
            $viewPath .= '.php';
        }
        ob_start();
        require(VIEW_PATH . DIRECTORY_SEPARATOR . $viewPath);
        $content = ob_get_clean();

        echo $content;
        return $content;
    }

    /**
     * @return mixed
     */
    public function getPdo()
    {
        if (!isset($this->pdo)) {

            $pdo = new PDO(getenv('DB_CONNECTION') .
                ':host=' . getenv('DB_HOST') .
                ';dbname=' . getenv('DB_DATABASE'), getenv('DB_USERNAME'), getenv('DB_PASSWORD')
            );
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo = $pdo;
        }

        return $this->pdo;
    }
}