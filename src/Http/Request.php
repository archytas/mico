<?php


namespace Mico\Http;


class Request
{
    const
        METHOD_GET = 'GET',
        METHOD_POST = 'POST',
        METHOD_PUT = 'PUT',
        METHOD_DELETE = 'DELETE';

    private $method;
    private $path;
    private $params;

    public function __construct($method = null, $path = null, $params = [])
    {
        $this->method = $method ?? $_SERVER['REQUEST_METHOD'];
        $this->path = $path ?? $_SERVER['REQUEST_URI'];

        // @todo: assign params properly
        $this->params = $params;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getParams()
    {
        return $this->params;
    }
}