<?php


namespace Mico\Http;


use Mico\Routing\Route;
use Mico\Routing\Router;

class Dispatcher
{
    /**
     * @var Router
     */
    private $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @param Request $request
     */
    public function dispatch(Request $request)
    {
        $route = $this->router->getMatchingRoute($request);

        if (!$route) {
            exit(404);
        }

        $controller = $this->loadController($route);
        call_user_func_array([$controller, $route->getControllerMethod()], $request->getParams());
    }

    /**
     * @param Route $route
     * @return mixed
     */
    public function loadController(Route $route)
    {
        $name = $route->getController();
        $file = CONTROLLER_PATH . DIRECTORY_SEPARATOR . $name . '.php';
        require($file);
        $namespacedName = 'App\\Controllers\\' . $name;
        $controller = new $namespacedName();
        return $controller;
    }
}