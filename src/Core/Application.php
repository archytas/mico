<?php

namespace Mico\Core;


use Mico\Http\Dispatcher;
use Mico\Http\Request;

class Application
{
    private $router;

    public function __construct($router)
    {
        $this->router = $router;
    }

    public function run()
    {
        $dispatcher = new Dispatcher($this->router);
        $dispatcher->dispatch(new Request());
    }
}