<?php


namespace Mico\Routing;


use http\Exception;
use Mico\Http\Request;

class Router
{

    /**
     * @var Router
     */
    private static $instance;

    /**
     * @var array
     */
    protected $routes = [];


    /**
     * @return Router
     */
    public static function getInstance(): self
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Add get route to the routes stack
     *
     * @param $path
     * @param $action
     * @throws \Exception
     */
    public function get($path, $action)
    {
        $this->addRoute(Request::METHOD_GET, $path, $action);
    }

    /**
     * @param $path
     * @param $action
     * @throws \Exception
     */
    public function post($path, $action)
    {
        $this->addRoute(Request::METHOD_POST, $path, $action);
    }

    /**
     * @param $method
     * @param $path
     * @param $action
     * @throws \Exception
     */
    public function addRoute($method, $path, $action)
    {
        if (!in_array($method, [Request::METHOD_GET, Request::METHOD_DELETE, Request::METHOD_POST, Request::METHOD_PUT])) {
            throw new \Exception("Invalid route method");
        }

        $this->routes[] = new Route($method, $path, $action);
    }

    /**
     * Return first matching route for request or null otherwise.
     *
     * @param Request $request
     * @return mixed|null
     */
    public function getMatchingRoute(Request $request)
    {
        foreach ($this->routes as $route) {

            if ($route->doesMatchRequest($request)) {
                return $route;
            }
        }

        return null;
    }
}