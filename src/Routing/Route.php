<?php


namespace Mico\Routing;


use Mico\Http\Request;

class Route
{

    protected $method;
    protected $path;
    protected $action;

    public function __construct($method = Request::METHOD_GET, $path = null, $action = null)
    {
        $this->method = $method;
        $this->path = rtrim($path, '/');
        $this->action = $action;
    }

    public function doesMatchRequest(Request $request)
    {
        if ($request->getMethod() !== $this->method) {
            return false;
        }

        // @todo: take parameterized routes into consideration
        if (rtrim($request->getPath(), '/') !== $this->path) {
            return false;
        }

        return true;
    }

    public function getController()
    {
        if (!isset($this->action)
            || strpos($this->action, '@') === false) {
            return null;
        }

        $parts = explode('@', $this->action);
        return $parts[0];
    }

    public function getControllerMethod()
    {
        if (!isset($this->action)
            || strpos($this->action, '@') === false) {
            return null;
        }

        $parts = explode('@', $this->action);

        return $parts[1];
    }
}