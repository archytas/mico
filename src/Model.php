<?php


namespace Mico;


class Model
{
    private $values = [];

    public function __construct($values = [])
    {
        $this->values = $values;
    }

    public function __get($key)
    {
        return $this->values[$key] ?? null;
    }
}