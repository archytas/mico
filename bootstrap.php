<?php

require_once __DIR__ . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'routes.php';

use Symfony\Component\Dotenv\Dotenv;
use Mico\Core\Application;

// load configuration variables into $_ENV
$dotenv = new Dotenv();
$dotenv->load(__DIR__ . DIRECTORY_SEPARATOR . '.env');

// helper path(s)
define('VIEW_PATH', __DIR__ . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'Views');
define('CONTROLLER_PATH', __DIR__ . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'Controllers');

$app = new Application($router);
return $app;