<?php
// better use twig or blade to embed stuff within the layout instead of the good old include stuff.
include VIEW_PATH . '/layouts/default/header.php';
include VIEW_PATH . '/layouts/partials/nav.php';
?>
    <header class="container">

        <div class="row">
            <div class="col-12">
                <?php if (!isset($user)) { ?>
                    <h1 class="mt-4">Welkom</h1>
                <?php } else { ?>
                    <h1 class="mt-4"><?php echo $pageTitle; ?> of <?php echo $user->name ?? 'no user'; ?></h1>
                    <p>Your reading list</p>
                <?php } ?>
            </div>
        </div>
    </header>

    <main class="container">
        <div class="row">
            <div class="col-md-8">

                <?php if (!(isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === 1)) { ?>
                    <div class="alert alert-danger">You have to <a href="/login">log in</a> to see your books collection.</div>
                <?php } else { ?>

                    <table id="books-table" class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Author</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($books as $book) { ?>
                            <tr data-id="<?php echo $book->id; ?>">
                                <th scope="row"><?php echo $book->id ?></th>
                                <td><?php echo $book->title ?></td>
                                <td><?php echo $book->author ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                    <div>Click a row in the table to see the book details.</div>

                <?php } ?>
            </div>
            <div class="col-md-4">
                <div id="book-summary" class="card">
                    <div class="card-body">
                        <h5 class="card-title book-title">Title</h5>
                        <p class="card-text author">text.</p>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <script>
        $("#book-summary").hide();

        $("#books-table tr").click(function() {
            let tableData = $(this).children("td").map(function() {
                return $(this).text();
            }).get();

            $("#book-summary .book-title").text(tableData[0]);
            $("#book-summary .author").text(tableData[1]);
            $("#book-summary").show();
        });
    </script>

<?php
include VIEW_PATH . '/layouts/default/footer.php';
