<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">M I C O</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="/">Book collection <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <?php if (!(isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === 1)) { ?>
                    <a class="nav-link" href="/login">Log in</a>
                <?php } else { ?>
                    <a class="nav-link" href="/login">Log out</a>
                <?php } ?>
            </li>
        </ul>
    </div>
</nav>