<?php


namespace App\Controllers;


use App\Models\Book;
use App\Models\User;

class BookController extends Controller
{

    private $loggedIn = false;

    public function __construct()
    {
        parent::__construct();

        if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === 1) {
            $this->loggedIn = 1;
        }
    }

    public function index()
    {
        $pageTitle = 'Books';

        $loggedIn = $this->loggedIn;
        $books = [];
        $user = null;

        if ($loggedIn) {
            $data = $this->getPdo()->query('SELECT * FROM books')->fetchAll();
            $books = array_map(function ($bookRecord) {
                return new Book($bookRecord);
            }, $data);

            $statement = $this->getPdo()->prepare('SELECT * FROM users WHERE id = ?');
            $statement->execute([$_SESSION['user_id']]);
            $data = $statement->fetch();
            $user = new User($data);
        }

        return $this->view('books/index', compact('user', 'pageTitle', 'books', 'loggedIn'));
    }

}