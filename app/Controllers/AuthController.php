<?php


namespace App\Controllers;


class AuthController extends Controller
{

    /**
     * @return string
     */
    public function login()
    {
        $failedLogin = false;

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $statement = $this->getPdo()->prepare('SELECT id FROM users WHERE email=? AND password=?');
            $password = hash('sha256', $_POST['password']);
            $statement->execute([$_POST['email'], $password]);

            if ($statement->rowCount() === 1) {
                $row = $statement->fetch();
                $_SESSION['logged_in'] = 1;
                $_SESSION['user_id'] = $row['id'];
                header('Location: /');
                exit;
            } else {
                $failedLogin = true;
            }
        }

        return $this->view('auth/login', compact('failedLogin'));
    }
}