<?php

use Mico\Routing\Router;

$router = Router::getInstance();
$router->get('/', 'BookController@index');
$router->get('/login', 'AuthController@login');
$router->post('/login', 'AuthController@login');