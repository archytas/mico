# Dump of table books
# ------------------------------------------------------------

DROP TABLE IF EXISTS `books`;

CREATE TABLE `books` (
                         `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                         `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                         `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                         `created_at` timestamp NULL DEFAULT NULL,
                         `updated_at` timestamp NULL DEFAULT NULL,
                         PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `books` WRITE;
/*!40000 ALTER TABLE `books` DISABLE KEYS */;

INSERT INTO `books` (`id`, `title`, `author`, `created_at`, `updated_at`)
VALUES
(1,'Grumpy Monkey','Suzanne Lang',NULL,NULL),
(2,'Monkey Wars','Richard Kurti',NULL,NULL);

/*!40000 ALTER TABLE `books` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
                         `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                         `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                         `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                         `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                         `is_admin` tinyint(4) NOT NULL DEFAULT '0',
                         `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
                         `created_at` timestamp NULL DEFAULT NULL,
                         `updated_at` timestamp NULL DEFAULT NULL,
                         `verified` tinyint(1) NOT NULL DEFAULT '0',
                         `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                         PRIMARY KEY (`id`),
                         UNIQUE KEY `users_email_unique` (`email`),
                         KEY `users_email_index` (`email`),
                         KEY `users_is_admin_index` (`is_admin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `email`, `password`, `is_admin`, `remember_token`, `created_at`, `updated_at`, `verified`, `verification_token`)
VALUES
(1,'Jaap Aap','j.aap@example.com','a98ec5c5044800c88e862f007b98d89815fc40ca155d6ce7909530d792e909ce',0,NULL,'2016-10-26 23:26:33','2016-10-26 23:26:33',1,NULL),
(2,'Mico Argentatus','mico@example.com','a98ec5c5044800c88e862f007b98d89815fc40ca155d6ce7909530d792e909ce',1,NULL,'2016-10-26 23:26:33','2016-10-26 23:26:33',1,NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;